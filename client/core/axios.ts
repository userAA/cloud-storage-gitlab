import axios from "axios";
import { parseCookies} from "nookies";

//адрес сервера
axios.defaults.baseURL = "http://localhost:7777";

axios.interceptors.request.use((config) => {
    if (typeof window !== "undefined")
    {
        //вытаскиваем token авторизованного пользователя из Cookies
        const { _token} = parseCookies();

        //в headers вводим  token
        config.headers.Authorization = "Bearer " + _token;
    }

    return config;
})

//экспортируем окончательный axios
export default axios;