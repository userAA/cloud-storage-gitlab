import React from "react"
import styles from "@/styles/Home.module.scss";
import {Upload, Button, UploadFile, notification} from "antd";
import {CloudUploadOutlined} from "@ant-design/icons";

import * as Api from "@/api";

//компонент загрузки на сервер информации о новом файле авторизованного пользователя
export const UploadButton: React.FC = () => {
    const [fileList, setFileList] = React.useState<UploadFile[]>([]);

    //функция указанной операции с файлом
    const onUploadSuccess = async (options : any) => {
        try
        {
            //отправляем выбранный файл на сервер
            const file = await Api.files.uploadFile(options);
            setFileList([]);
            window.location.reload();
        }
        catch (err)
        {
            //отправить выбранный файл на сервер не удалось
            notification.error({
                message: "Error!",
                description: "Failed to upload file",
                duration: 2
            });
        }
    }

    return (
        //компонент загрузки файла с диска и отправки его информации на сервер
        <Upload
            customRequest={onUploadSuccess}
            fileList={fileList}
            onChange={({fileList}) => setFileList(fileList)}
            className={styles.upload}
        >
            {/*кнопка загрузки файла с диска и отправки его информации на сервер*/}
            <Button type="primary" icon={<CloudUploadOutlined/>} size="large">
                Upload a file
            </Button>
        </Upload>
    )
}