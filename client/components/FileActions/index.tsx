import React from "react";
import styles from "./FileActions.module.scss";
import { Button, Popconfirm } from "antd";

interface FileActionsProps 
{
    //функция удаления выделенных файлов
    onClickRemove: VoidFunction,
    //флаг, говорящий о том, что существуют выделенные файлы
    isActive: boolean
}

//компонент действий с выделенными файлами авторизованного пользователя
export const FileActions: React.FC<FileActionsProps> = ({
    onClickRemove,
    isActive
}) => {
    return (
        <div className={styles.root}>
            {/*Компонент отправки в корзину выделенных файлов */}
            <Popconfirm
                title="Delete file(s)"
                description="All files will be moved to the trash"
                okText="Yes"
                cancelText="No"
                disabled={!isActive}
                onConfirm={onClickRemove}
            >
                {/*Функция отправки в корзину выделенных файлов */}
                <Button disabled={!isActive} type="primary" danger>
                    Remove
                </Button>
            </Popconfirm>
        </div>
    )
}