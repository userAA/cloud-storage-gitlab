import React from 'react';
import {setCookie} from "nookies";
import styles from './Auth.module.scss';
import { Form, Input, Button, notification } from 'antd';
import { LoginFormDTO } from '@/api/dto/auth.dto';

import * as Api from '../../api';

//компонент авторизации зарегестрированного пльзователя
export const LoginForm: React.FC = ({}) => 
{
    //обработчик события проведения процесса авторизации зарегистрированного пользователя
    const onSubmit = async (values: LoginFormDTO) => {
        try {
            //получаем готовый токен полученный в результате осуществления указанного процесса авторизации
            const {token} = await Api.auth.login(values);

            //появляется памятка о том, что проведенный запрос осуществился успешно
            notification.success({
                message: "Successfully!",
                description: "Go to the admin panel...",
                duration: 2
            });

            //устанавливаем полученный токен в cookies
            setCookie(null, "_token", token, {
                path: "/"
            });

            //осуществляем переход на основную страницу проекта
            location.href = "/dashboard";
        } 
        catch (err) 
        {
            //осуществляем переход на основную страницу проекта
            notification.error({
                message: "Error!",
                description: "Invalid login or password",
                duration: 2
            })
        }
    }

    return (
        <div className={styles.formBlock}>
            <Form
                name='basic'
                labelCol={{
                    span: 8
                }}
                onFinish={onSubmit}
            >
                {/*Поле задания электоронной почты авторизуемого пользователя */}
                <Form.Item
                    label="E-Mail"
                    name="email"
                    rules={[
                        {
                            required: true,
                            message:  "Specify the email address"
                        }
                    ]}
                >
                    <Input/>
                </Form.Item>

                {/*Поле задания пароля авторизуемого пользователя */}
                <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: "Specify the password"
                        }
                    ]}
                >
                    <Input.Password/>
                </Form.Item>

                <Form.Item
                    wrapperCol= {{
                        offset: 8,
                        span: 16
                    }}
                >
                    {/*Кнопка запуска обработчика события авторизации пользователя */}
                    <Button type="primary" htmlType="submit">
                        Enter
                    </Button>
                </Form.Item>
            </Form>
        </div>
    )
}