import React from "react";
import styles from "./FileList.module.scss";
import { fileItem } from "@/api/dto/files.dto";
import { FileCard } from "@/components/FileCard";
import Selecto from "react-selecto";

export type FileSelectType = "select" | "unselect";

interface FileListProps {
    //шаблон списка всех файлов авторизованного пользователя
    items: fileItem[];
    //функция выделения любого файла авторизованного пользователя
    onFileSelect: (id: number, type: FileSelectType) => void;
}

//компонент показа всех файлов авторизованного пользователя
export const FileList: React.FC<FileListProps> = ({ items, onFileSelect }) => {
    return (
        <div className={styles.root}>
            {items.map((item) => (
                //показываем каждый файл авторизованного пользователя
                <div data-id={item.id} key={item.id} className="file">
                    <FileCard filename={item.filename} originalName={item.originalName} />
                </div>
            ))}

            {/*компонент выделения файлов  */}
            <Selecto
                container=".files"
                selectableTargets={[".file"]}
                selectByClick
                hitRate={10}
                selectFromInside
                toggleContinueSelect={["shift"]}
                continueSelect={false}
                onSelect={(e) => {
                    e.added.forEach((el) => {
                        el.classList.add("active");
                        onFileSelect(Number(el.dataset["id"]), "select");
                    });
                    e.removed.forEach((el) => {
                        el.classList.remove("active");
                        onFileSelect(Number(el.dataset["id"]), "unselect");
                    });
                }}
            />
        </div>
    );
};