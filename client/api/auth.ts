import axios from "../core/axios";
import {
    //вытаскиваем интерфейс данных для авторизации зарегистрированного пользователя
    LoginFormDTO, 
    //вытаскиваем интерфейс данных о результате авторизации зарегистрированного пользователя
    LoginResponseDTO,
    //вытаскиваем интерфейс данных для регистрации пользователя
    RegisterFormDTO,
    //вытаскиваем интерфейс данных о результате регистрации пользователя
    RegisterResponseDTO,
    //вытаскиваем интерфейс данных о зарегистрированном и авторизованном пользователе
    User
} from "../api/dto/auth.dto";
import {destroyCookie} from "nookies";

//функция запроса на авторизацию зарегистрированного пользователя
export const login = async (values : LoginFormDTO): Promise<LoginResponseDTO> => {
    //запрос на авторизацию зарегистрированного пользователя
    return ( await axios.post("/auth/login", values)).data;
};

//функция запроса на регистацию пользователя
export const register = async (values : RegisterFormDTO): Promise<RegisterResponseDTO> => {
    //запрос на регистрацию пользователя
    return ( await axios.post("/auth/register", values)).data;
};
 
//функция запроса на получение полной информации об авторизированном пользователе 
export const getMe = async(): Promise<User> => {
    //запрос на получение полной информации об авторизированном пользователе 
    return (await axios.get("/users/me")).data;
};

//функция устранение информации об авторизированном пользователе из cookie
export const logout = () => {
    destroyCookie(null, "_token", {path: "/"});
};