import {User} from "@/api/dto/auth.dto";

//шаблон полной информации о файле
export interface fileItem {
    filename: string;
    originalName: string;
    size: number;
    mimetype: string;
    user: User;
    deleteAt: string | null;
    id: number;
}