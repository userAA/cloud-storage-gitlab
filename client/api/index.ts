//экспортируем запросы, связанные с пользователем
export * as auth from "./auth";

//экспортируем запросы, связанные с файлами
export * as files from "./files";