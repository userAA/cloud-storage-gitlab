import Head from"next/head"
import { Header } from "@/components/Header"
import React from "react"

import styles from "@/styles/Home.module.scss";

interface LayoutProps {
    title: string;
}

//web page template
//шаблон веб-страницы
export const Layout: React.FC<React.PropsWithChildren<LayoutProps>> = ({
    //name of the web page
    //название web-страницы    
    title,
    //components of the main part of the web page
    //компоненты основной части web-страницы
    children
}) => {
    return (
        <>
            {/*Title of the web page*/}
            {/*Заголовок web-станицы */}
            <Head>
                <title>{title}</title>
            </Head>
            <main>
                {/*The main part of the web page*/}
                {/*Заглавная часть web-страницы */}
                <Header />
                {/*The main part of the web page*/}
                {/*Основная часть web-страницы */}
                <div className={styles.main}>
                    <div className={styles.layout}>{children}</div>
                </div>        
            </main>
        </>
    )
}