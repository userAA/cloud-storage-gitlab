import React from "react";
import styles from "@/styles/Home.module.scss";
//вытаскиваем хук маршрутизатора
import { useRouter } from "next/router";
//вытаскиваем кнопку загрузки файлов с диска
import {UploadButton} from "@/components/UploadButton";
import {Menu} from "antd";
import {
    DeleteOutlined,
    FileImageOutlined,
    FileOutlined
} from "@ant-design/icons";

//компонент операций с файлами авторизованного пользователя
export const DashboardLayout: React.FC<React.PropsWithChildren> = ({
   children 
}) => {
    
    //формируем маршрутизатор из имеющегося хука маршрутизаторов
    const router = useRouter();

    //определяем название ключа выделенного компонента меню показа всех файлов
    const selectedMenu = router.pathname;

    return (
        <main className={styles.dashboardContainer}>
            <div className={styles.sidebar}>
                {/*Кнопка загрузки нового файла */}
                <UploadButton />
                <Menu 
                    className={styles.menu}
                    mode="inline"
                    selectedKeys={[selectedMenu]}
                    items={[
                        //Клавиша показа всех файлов авторизованного пользователя
                        {
                            key: `/dashboard`,
                            icon: <FileOutlined />,
                            label: `Files`,
                            //событие включения маршрутизатора перехода на показ 
                            //всех загруженных файлов авторизованным пользователем 
                            onClick: () => router.push("/dashboard")
                        },
                        //Клавиша показа файлов изображений авторизованного пользователя
                        {
                            key: `/dashboard/photos`,
                            icon: <FileImageOutlined />,
                            label: `Photos`,
                            //событие включения маршрутизатора перехода на показ 
                            //всех загруженных файлов-изображений авторизованным пользователем 
                            onClick: () => router.push("/dashboard/photos")
                        },
                        //Клавиша показа файлов, помещенных авторизованным пользователем в корзину
                        {
                            key: `/dashboard/trash`,
                            icon: <DeleteOutlined />,
                            label: `Trash`,
                            //событие включения маршрутизатора перехода на показ 
                            //всех помещенных в корзину файлов авторизованным пользователем 
                            onClick: () => router.push("/dashboard/trash")
                        },
                    ]}
                />
            </div>

            {/*Компонент показа  файлов (файлы можно выделять, помещать в козину и выбирать из них только изображения)  */}
            <div className="container">{children}</div>
        </main>
    )
}