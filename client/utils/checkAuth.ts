import { GetServerSidePropsContext } from "next";
import nookies from "nookies";
import axios from "../core/axios";
import * as Api from "../api";

//функция проверки существования авторизации пользователя 
export const checkAuth = async (ctx: GetServerSidePropsContext) => {
    //вытаскиваем token авторизованного пользователя из cookies если такой пользователь существует
    const {_token} = nookies.get(ctx);

    //закачиваем полученый из nookies token в axios
    axios.defaults.headers.Authorization = "Bearer " + _token;

    try {
        //посылаем запрос на получение всех данных по авторизованному пользователю, если такой пользователь существует
        await Api.auth.getMe();

        return {
            //указанный запрос осуществился успешно
            props: {}
        };
    } catch (err) {
        return {
            //указанный запрос не получился, делаем перенаправление на страницу авторизации зарегистрированного пользователя
            redirect: {
                destination: "/dashboard/auth",
                permanent: false
            }
        }
    }
}