import { Extension } from "./getColorByExtension";

//функция определения расширения файла
export const getExtensionFromFileName = (filename: string) => {
    return filename.split(".").pop() as Extension;
}