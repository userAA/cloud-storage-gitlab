import React from "react";
import {fileItem} from "@/api/dto/files.dto";
import {FileActions} from "@/components/FileActions";
import {FileList, FileSelectType} from "@/components/FileList";
import { Empty} from "antd";

import * as Api from "@/api";

interface FilesProps {
    items: fileItem[];
    withActions?: boolean;
}

//компонент содержания файлов авторизованного пользоватяеля
export const Files: React.FC<FilesProps> = ({items, withActions}) => {
    //состояние списка файлов, отправляемых в корзину, авторизованным пользователем
    const [files, setFiles] = React.useState(items || []);
    //состояние списка идентификаторов по выделенным файлам авторизованного пользователя
    const [selectedIds, setSelectedIds] = React.useState<number[]>([]);

    //функция выделения файлов авторизованного пользователя
    const onFileSelect = (id: number, type: FileSelectType) => {
        if (type === "select")
        {
            //выделение файлов происходит при помощи создания рамки
            setSelectedIds((prev) => [...prev, id]);
        }
        else
        {
            //выделение файлов происходит при помощи нажатия на файл  
            setSelectedIds((prev) => prev.filter((_id) => _id !== id));
        }
    } 

    //функция пометки выделенных файлов как удаленных
    const onClickRemove = () => {
        //список идентификаторов выделенных файлов очищаем
        setSelectedIds([]);
        //формируем список файлов, отправляемых в корзину
        setFiles((prev) => prev.filter((file) => !selectedIds.includes(file.id)));
        //we make a request to send the corresponding list of files to the trash
        Api.files.remove(selectedIds as any);
    }
    
    return (
        <div>
            {files.length ? (
                <>
                    {withActions && 
                        //компонент отправки выделенных файлов в корзину
                        <FileActions
                            onClickRemove={onClickRemove}
                            isActive={selectedIds.length > 0}
                        />
                    }
                    {/*Сам список файлов авторизованного пользователя с функцией их выделения */}
                    <FileList items={files} onFileSelect={onFileSelect}/>
                </>
            ) : (
                <Empty className="empty-block" description="The file list is empty" />    
            )}
        </div>
    )
}