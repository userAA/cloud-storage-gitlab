import {NextPage} from 'next';
import Head from 'next/head';
//вытаскиваем компонент авторизации пользователя
import {LoginForm} from '../../components/auth/LoginForm';
//вытаскиваем компонент регистрации пользователя
import {RegisterForm} from '../../components/auth/RegisterForm';

import {Tabs} from 'antd'

//страница регистрации и авторизации пользователя
const AuthPage: NextPage = () => {
    return (
        <>
            {/*Заголовок станицы*/}
            <Head>
                <title>Dashboard / Auth</title>
            </Head>
            <main style={{width: 400, margin: '50px auto'}} >
                <Tabs
                    items = {[ 
                        //клавиша авторизации пользователя
                        {
                            label: 'Enter',
                            key: '1',
                            children: <LoginForm/>
                        },
                        //клавиша регистрации пользователя
                        {
                            label : 'Register',
                            key: '2',
                            children: <RegisterForm/>
                        }
                    ]} 
                />
            </main>
        </>
    )
}

export default AuthPage;