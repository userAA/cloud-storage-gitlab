import {GetServerSidePropsContext, NextPage} from "next";
//импотируем шаблон информации об авторизованном пользователе
import {User} from '../../api/dto/auth.dto';

import {Button} from "antd";
import {Layout} from "../../layouts/Layout"

import styles from '@/styles/Profile.module.scss'
//импортируем функцию проверки существования авторизованного пользователя
import {checkAuth} from "../../utils/checkAuth"; 
import * as Api from "../../api";

//шаблон информации об авторизованном пользователе
interface Props {
    userData: User;
}

//страница показа информации об авторизованном пользователе
export const DashboardProfilePage: NextPage<Props> = ({userData}) => 
{
    //обработчик события удаления информации из cookie token авторизованного пользователя
    const onClickLogout = () => {
        if (window.confirm("Do you really want to get out?"))
        {
            //удаляем token авторизации пользователя из cookie
            Api.auth.logout();
            //переходим на центральную страницу проекта
            location.href = "/dashboard";
        }
    }

    return (
        <main>
            <div className={styles.root}>
                <h1>My profile</h1>
                <br />
                <p>
                    {/*Идентификатор авторизованного пользователя */}
                    ID: <b>{userData.id}</b>
                </p>
                <p>
                    {/*Полное имя авторизованного пользователя */}
                    Full name: <b>{userData.fullName}</b>
                </p>
                <p>
                    {/*Электронная почта авторизованного пользователя */}
                    E-Mail: <b>{userData.email}</b>
                </p>
                <br/>
                {/* Кнопка снятия авторизации с автормзованного пользователя*/}
                <Button onClick={onClickLogout} type="primary" danger>
                    Escape                    
                </Button>
            </div>
        </main>
    )
}

DashboardProfilePage.getLayout = (page: React.ReactNode) => {
    //задаем страницы показа информации об авторизованном пользователе
    return <Layout title="Dashboard / Profile" >{page}</Layout>
}

//функция получения props по странице показа данных авторизованного пользователя
export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
    //проверяем существует ли авторизованный пользователь
    const authProps = await checkAuth(ctx);

    //если нет то производим перенаправление на страницу авторизации зарегистированного пользователя 
    if ("redirect" in authProps) {
        return authProps;
    }

    //осуществляем запрос на получение всех данных по авторизованному пользователю
    const userData = await Api.auth.getMe();

    return {
        //формируем искомые props
        props: {
            userData
        }
    }
}

export default DashboardProfilePage;