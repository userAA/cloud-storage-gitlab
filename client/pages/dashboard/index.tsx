import {GetServerSidePropsContext, NextPage} from "next";
//импортируем функцию проверки существования авторизованного пользователя
import { checkAuth } from "@/utils/checkAuth";
import React from "react";
//импотируем полный шаблон web-страницы
import { Layout } from "../../layouts/Layout";
//импортируем компонент операций с файлами авторизованного пользователя
import { DashboardLayout } from "@/layouts/DashboardLayout";

import * as Api from "@/api";
//вытаскиваем шаблон данных по файлам
import {fileItem} from "@/api/dto/files.dto";
//вытаскиваем компонент показа файлов авторизованного пользователя
import { Files } from "@/modules/Files";

//формируем шаблон props основной страницы проекта
interface Props {
    items: fileItem[];
}

//шаблон основной страницы проекта
const DashboardPage: NextPage<Props> = ({items}) => {
    return  (
        //компонент операций с файлами авторизованного пользователя
        <DashboardLayout>
            {/*Компонент показа файлов авторизованного пользователя */}
            <Files items={items} withActions/>
        </DashboardLayout>
    )
}

DashboardPage.getLayout = (page: React.ReactNode) => {
    //задаем заголовок основной страницы проекта 
    return <Layout title="Dashboard / Main">{page}</Layout>
}

//функция получения props по основной странице проекта
export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
    //получаем данные по авторизованному пользователю
    const authProps = await checkAuth(ctx);

    //если есть redirect в полученных данных, то переходим на страницу регистрации и авторизации пользователя
    if ('redirect' in authProps) {
        return authProps;
    }

    try {
        //получаем все файлы авторизованного пользователя
        const items = await Api.files.getAll();

        return {
            props: {
                //формируем искомые props
                items
            }
        }
    }
    catch (err) {
        console.log(err);
        //в случае неудачного запроса 
        return {
            props: {items : []}
        }
    }
}

export default DashboardPage;