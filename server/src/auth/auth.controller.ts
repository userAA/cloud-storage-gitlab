import { Body, Controller, Post, Request, UseGuards } from '@nestjs/common';
import { ApiBody} from '@nestjs/swagger';
import { CreateUserDto} from '../users/dto/create-user.dto';
import { AuthService } from './auth.service';
import { UserEntity } from 'src/users/entities/user.entity';
import { LocalAuthGuard } from './guards/local.guard';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}
 
    //проверяем существует ли зарегистрированный пользователь, которого нужно авторизовать 
    //(при этом связываемся с функцией LocalStrategy.validate) 
    @UseGuards(LocalAuthGuard)
    //проводим запрос на авторизацию зарегистрированного пользователя
    @Post('/login')
    //задаем шаблон входных данных по авторизации зарегистрированного пользователя 
    @ApiBody({type: CreateUserDto})
    login(@Request() req: any) 
    {
        //проводим требуемую авторизацию
        return this.authService.login(req.user as UserEntity);
    }

    //запрос на регистрацию нового пользователя
    @Post('/register')
    register(@Body() dto: CreateUserDto) {
        //проводим требуемую регистрацию
        return this.authService.register(dto);
    }
}
