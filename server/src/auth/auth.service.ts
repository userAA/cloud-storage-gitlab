import { ForbiddenException, Injectable } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UserEntity } from 'src/users/entities/user.entity';
import {JwtService} from '@nestjs/jwt'

@Injectable()
export class AuthService {
    constructor(
        //сервис по данным пользователя
        private usersService: UsersService,
        //серфис по формированию токенов
        private jwtService: JwtService
    ) {}

    //функция проверки существования пользователя по email  и password
    async validateUser(email: string, password: string) : Promise<any> {
        //ищем пользователя по email в базе данных пользователей
        const user = await this.usersService.findByEmail(email);

        //если пользователь найден то сравниваем его пароль с входным паролем
        if (user && user.password === password)
        {
            const {password, ...result} = user;
            //в случае успеха возвращаем искомый результат
            return result;
        }

        //в противном случае возвращается нулевой результат
        return null;
    }

    //функция регистрации нового пользователя
    async register(dto: CreateUserDto)
    {
        try
        {
            //проводим указанную регистрацию
            const userData = await this.usersService.create(dto);
            return userData;
        } 
        catch (err)
        {
            console.log(err);
            throw new ForbiddenException('Error during registration');
        }
    }

    //функция авторизации зарегистрированого пользователя
    //(тут идет связь с функцией JwtStrategy.validate )
    async login(user: UserEntity) {
        return {
            //получаем токен авторизации зарегистрированного пользователя
            token: this.jwtService.sign({id: user.id})
        }
    }
}