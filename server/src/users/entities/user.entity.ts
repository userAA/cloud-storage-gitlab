import {Column, Entity, PrimaryGeneratedColumn, OneToMany} from 'typeorm';
import { FileEntity } from 'src/files/entities/file.entity'; 

@Entity('users')
//схема базы данных по пользователю
export class UserEntity {
    //идентификатор пользователя
    @PrimaryGeneratedColumn()
    id: number;

    //электронная почта пользователя
    @Column()
    email: string;

    //пароль пользователя
    @Column()
    password: string;

    //полное имя пользователя
    @Column()
    fullName: string;

    //связь с файлами (один пользователь может содержать множаество файлов)
    @OneToMany( () => FileEntity, (file : FileEntity) => file.user)
    files: FileEntity[];
}