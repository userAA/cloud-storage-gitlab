import { ApiProperty} from '@nestjs/swagger'

//шаблон данных по пользователю
export class CreateUserDto {
    //электронная почта пользователя
    @ApiProperty({
        default: 'test@test.ru'
    })
    email: string;

    //полное имя пользователя
    @ApiProperty({
        default: 'Мистер Кредо'
    })
    fullName: string;

    //пароль пользователя
    @ApiProperty({
        default: '123'
    })
    password: string;
}