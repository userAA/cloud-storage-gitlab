import { Injectable } from '@nestjs/common';
import { InjectRepository} from "@nestjs/typeorm"
import { Repository} from 'typeorm';
import { UserEntity } from './entities/user.entity';
import { CreateUserDto } from './dto/create-user.dto';

@Injectable()
export class UsersService {
  constructor (
    @InjectRepository(UserEntity)
    private repository: Repository<UserEntity>
  ) {}

  //функция определения данных по автоизованному пользователю на основании вводимой электронной почты email
  async findByEmail(email: string) {
    return this.repository.findOneBy({
      email
    });
  }

  //функция определения данных по автоизованному пользователю на основании вводимого идентификатора id
  async findById(id: number) {
    return this.repository.findOneBy({
      id
    })    
  }

  //функция добавления в базу данных информации о новом зарегистрированном пользователе
  async create(dto : CreateUserDto) {
    return await this.repository.save(dto);
  }
}
