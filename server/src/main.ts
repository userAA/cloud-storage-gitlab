import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as express from 'express';
import { join } from 'path';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {cors: false});

  //we allow requests from other urls
  app.enableCors({credentials: true, origin: true}); 

  //creating the uploads folder where we will upload files of authorized users
  app.use('/uploads', express.static(join(__dirname, '..', 'uploads')));

  //starting the server
  await app.listen(7777);
}
bootstrap();
