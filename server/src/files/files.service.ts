import { Injectable } from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {FileEntity, FileType} from './entities/file.entity';
import {Repository} from 'typeorm'

@Injectable()
//класс операций с файлами
export class FilesService {
  constructor(
    @InjectRepository(FileEntity)
    private repository: Repository<FileEntity>
  ) {}

  //функция получения файлов пользователя с идентификатором userId
  findAll(userId: number, fileType: FileType) {
    const qb = this.repository.createQueryBuilder('file');

    //получаем все указанные файлы
    qb.where('file.userId = :userId', { userId });

    if (fileType === FileType.PHOTOS) {
      //среди всех полученных файлов, выбираем только те, которые являются изображениями
      qb.andWhere('file.mimetype ILIKE :type', { type: '%image%' });
    }

    if (fileType === FileType.TRASH) {
      //среди полученных файлов выбираем только те, которые находятся в корзине
      qb.withDeleted().andWhere('file.deletedAt IS NOT NULL');
    }

    return qb.getMany();
  }

  //в базу данных отправляем информацию о новом созданном файле
  create(file: Express.Multer.File, userId: number)
  {
    return this.repository.save({
      //сформированное имя файла
      filename: file.filename,
      //первоначальное имя файла
      originalName: file.originalname,
      //размер файла
      size: file.size,
      //тип файла (текстовый или пиксельный)
      mimetype: file.mimetype,
      //идентификатор пользователя, который загрузил этот файл с диска
      user: {id : userId}
    });
  }

  //функция помещения в козину серии файлов пользоватяля с идентификатором userId
  async remove(userId: number, ids: string)
  {
    //выделяем идентификаторы файлов, которые нужно поместить в корзину
    const idsArray = ids.split(',');
    
    const qb = this.repository.createQueryBuilder('file');

    //выполняем указанный процесс
    qb.where('id IN (:...ids) AND userId = :userId', {
      ids: idsArray,
      userId
    });

    //выводим результат проведенной операции
    return qb.softDelete().execute();
  }
}