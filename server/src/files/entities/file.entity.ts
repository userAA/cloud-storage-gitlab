import {
    Column, 
    DeleteDateColumn, 
    Entity, 
    PrimaryGeneratedColumn, 
    ManyToOne
} from 'typeorm'
import { UserEntity } from 'src/users/entities/user.entity';

//типы файлов
export enum FileType {
    //фотографии
    PHOTOS = 'photos',
    //файлы в корзине
    TRASH = 'trash'
}

@Entity('files')
//схема базы данных по файлу
export class FileEntity {
    //идентификатор файла
    @PrimaryGeneratedColumn()
    id: number;

    //сформированное имя файла
    @Column()
    filename: string;

    //первоначальное имя файла
    @Column()
    originalName: string;

    //размер файла
    @Column()
    size: number;

    //тип файла
    @Column()
    mimetype: string;

    //пользователь, который загрузил файл (одному пользователю может соответствовать множество файлов)
    @ManyToOne(() => UserEntity, (user: UserEntity) => user.files)
    user: UserEntity;

    //дата помещения файла в корзину, если это состоялось
    @DeleteDateColumn()
    deletedAt?: Date;
}
